<?php 

## Exercice 5 En allant de 1 à 15 avec un pas de 1, afficher le message On y arrive presque.

## Exercice 6 En allant de 20 à 0 avec un pas de 1, afficher le message C'est presque bon.

## Exercice 7 En allant de 1 à 100 avec un pas de 15, afficher le message On tient le bon bout.

## Exercice 8 En allant de 200 à 0 avec un pas de 12, afficher le message Enfin !!!!.

//EX 5:
for($i=1; $i <= 15; $i++) 
{
    echo $i .'</br>';
    echo "On y arrive presque...".'</br>';
}

//EX 6:
$a= 20;
while($a > 0)
{
    echo --$a .'</br>';
    echo "C'est presque bon...".'</br>';
}

// EX 7:
$b= 1;
echo $b .'</br><hr>';
while($b < 100)
{
    echo $b = $b + 15 .'</br>';
    echo "On tient le bout !".'</br>';
    if ($b > 100) { 
        echo "</br>Tu as voulu tricher, ça dépasse 100 ;)<hr>";
    }
    else {
        echo "Tu as vu juste, $b est en dessous de 100 </br><hr>";
    }
}

// EX8:

$c = 200;
echo $c . '</br><hr>';
while($c > 0)
{
    echo $c = $c - 12 .'</br>';
    echo "Enfiiiin !!!".'</br>';
    if ($c < 0) { 
        echo "</br>Tu as voulu tricher, ça dépasse les 0 ;)<hr>";
    }
    else {
        echo "Tu as vu juste, $c est au dessus de 0 </br><hr>";
    }
}
?>